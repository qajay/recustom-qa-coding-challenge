import { Page, Locator } from '@playwright/test';

/**
 * The dashboard class provides methods and properties for interacting with
 * dashboard page in your application.
 */
class dashboard {
  private page: Page;

  /** Locator for create  */
  public readonly Elements: {
    createHaggadah: Locator;

  };

  /**
   * Initializes a new instance of the dashboard class.
   *
   * @param page - The Playwright Page instance.
   */
  constructor(page: Page) {
    this.page = page;
    this.Elements = {
      createHaggadah: page.locator("(//div[@class='flex items-center justify-center h-full'])[11]"),

    };
  }


}

export default dashboard;
