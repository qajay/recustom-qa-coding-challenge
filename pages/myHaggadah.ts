import { Page, Locator, expect } from '@playwright/test';

/**
 * The myHaggadah class provides methods and properties for interacting with
 * myHaggadah page in your application.
 */
class myHaggadah {
  private page: Page;

  /** Locator for the elements of the myHaggadah page. */
  public readonly Elements: {
    liberal: Locator;
    getStarted: Locator;
    save: Locator;
    enterName: Locator;
    
  };

  /**
   * Initializes a new instance of the myHaggadah class.
   *
   * @param page - The Playwright Page instance.
   */
  constructor(page: Page) {
    this.page = page;
    this.Elements = {
      liberal: page.locator("((//div[@class='text-2xl font-bold text-black font-tthoves-bold']))[1]"),
      getStarted: page.locator("((//div[@class='flex items-center justify-center h-full']))[12]"),
      save: page.locator("((//div[@class='flex items-center justify-center h-full']))[12]"),
      enterName: page.locator("//input[@placeholder='Enter title here']")
      
      
    };
  }

  
}

export default myHaggadah;
