import { Locator, Page } from '@playwright/test';

class LoginPage {
  private page: Page;
  private emailInputField: Locator;
  private passwordInputField: Locator;
  private loginBtn: Locator;

  constructor(page: Page) {
    // Initialize page and locators for elements on the login page
    this.page = page;
    this.emailInputField = page.locator("//input[@placeholder='Enter your account email']");
    this.passwordInputField = page.locator("//input[@placeholder='Enter your account password']");
    this.loginBtn = page.locator("(//div[@class='flex items-center justify-center h-full'])[3]");
  }

  // Method to navigate to the login page
  async visitLogin() {
    await this.page.goto('user/login'); // Assuming the login page URL is 'user/login'
    await this.page.waitForLoadState(); // Wait for the page to load
  }

  // Method to perform login
  async performLogin({ email, password }: { email: string; password: string }) {
    // Fill in email and password fields, then click on the login button
    await this.emailInputField.fill(email);
    await this.passwordInputField.fill(password);
    await this.loginBtn.click();

    // Wait for the page to finish loading and ensure the URL is redirected to the dashboard
    await this.page.waitForLoadState();
    await this.page.waitForURL('https://www.haggadot.com/account/dashboard');
  }
}

export default LoginPage;
