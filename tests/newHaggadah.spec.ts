import { test, expect } from '@playwright/test';
import dashboard from '../pages/dashboard';
import myHaggadah from '../pages/myHaggadah';
import LoginPage from '../pages/login';
import * as dotenv from 'dotenv';

// Load environment variables from .env file
dotenv.config();

test.describe('Create new Haggadah', () => {
 
  // Test for creating new Haggadah
  test('Should be able to create new haggadah', async ({ page }) => {
    // Instantiate page objects
    const dashBoard = new dashboard(page);
    const myhaggadah = new myHaggadah(page);
    const signin = new LoginPage(page);

    // Navigate to the login page
    await signin.visitLogin();

    // Perform login using provided credentials
    await signin.performLogin({
      email: process.env.EMAIL as string,
      password: process.env.PASSWORD as string,
    });

    // Click on the 'Create Haggadah' button
    await dashBoard.Elements.createHaggadah.click();

    // Select the 'Liberal' option
    await myhaggadah.Elements.liberal.click();

    // Click on the 'Get Started' button
    await myhaggadah.Elements.getStarted.click();

    // Click on the 'Save' button
    await myhaggadah.Elements.save.click();
  });
});
