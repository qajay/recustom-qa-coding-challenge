# Automation Testing Framework

[This framework is based on Page Object Model]

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)

## Installation

To get started with this project, follow these steps:

1. Clone the repository:
   ```
   git clone https://github.com/your/repository.git
   ```

2. Install dependencies:
   ```
   npm install
   ```

## Usage

To run tests for this project, use the following command:
```
npx playwright test --ui

```

## Test case automated
-Login
-Create new Haggadah

This command will execute and open a UI to run test.
Run the test present there using the play button on the test.

